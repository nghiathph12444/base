package com.base.app.data;

import com.base.app.data.prefs.PreferencesHelper;
import com.base.app.data.realm.RealmHelper;

/**
 * Created by GNUD on 02/12/2017.
 */

public interface DataManager extends PreferencesHelper, RealmHelper {

}
