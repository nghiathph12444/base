package com.base.app.ui.main

import androidx.lifecycle.ViewModelProviders
import com.base.app.R
import com.base.app.view_model.ViewModelFactory
import com.base.app.base.BaseActivity
import com.base.app.databinding.ActivityMainBinding


class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    override fun getContentLayout(): Int {
        return R.layout.activity_main
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProviders.of(this, ViewModelFactory(this)).get(MainViewModel::class.java)
    }

    override fun initView() {

    }

    override fun initListener() {

    }

    override fun observerLiveData() {
    }
}
