package com.base.app.ui.splash

import androidx.lifecycle.ViewModelProviders
import com.base.app.R
import com.base.app.ui.login.LoginViewModel
import com.base.app.view_model.ViewModelFactory
import com.base.app.base.BaseActivity
import com.base.app.databinding.ActivitySplashBinding

class SplashActivity : BaseActivity<LoginViewModel, ActivitySplashBinding>() {

    override fun getContentLayout(): Int {
        return R.layout.activity_splash
    }

    override fun initViewModel() {
        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(LoginViewModel::class.java)
    }

    override fun initView() {
    }

    override fun initListener() {
    }

    override fun observerLiveData() {
        viewModel.apply {

        }
    }
}
