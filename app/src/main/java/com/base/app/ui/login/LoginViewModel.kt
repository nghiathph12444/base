package com.base.app.ui.login

import android.annotation.SuppressLint
import com.base.app.data.DataManager
import com.base.app.network.Api
import com.base.app.view_model.BaseViewModel
import javax.inject.Inject

@SuppressLint("CheckResult")
class LoginViewModel(): BaseViewModel() {
    @Inject
    lateinit var dataManager: DataManager
    @Inject
    lateinit var api: Api

}