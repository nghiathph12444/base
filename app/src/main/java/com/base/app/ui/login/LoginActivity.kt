package com.base.app.ui.login

import androidx.lifecycle.ViewModelProviders
import com.base.app.R
import com.base.app.view_model.ViewModelFactory
import com.base.app.base.BaseActivity
import com.base.app.databinding.ActivityLoginBinding

class LoginActivity : BaseActivity<LoginViewModel, ActivityLoginBinding>() {

    override fun getContentLayout(): Int {
        return R.layout.activity_login
    }

    override fun observerLiveData() {
        viewModel.apply {
        }
    }

    override fun initView() {
    }

    override fun initListener() {

    }

    override fun initViewModel() {
        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(LoginViewModel::class.java)
    }

}
