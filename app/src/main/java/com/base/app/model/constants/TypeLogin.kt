package com.base.app.model.constants

enum class TypeLogin(val value: Int) {
    MANUAL(0),
    FACEBOOK(1),
    GOOGLE(2)
}