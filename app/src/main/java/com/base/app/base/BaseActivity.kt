package com.base.app.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.SystemClock
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.snackbar.Snackbar
import com.base.app.view_model.BaseViewModel
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import java.lang.reflect.Field
import java.lang.reflect.Method


abstract class BaseActivity<VM : BaseViewModel, BINDING : ViewDataBinding> :
    AppCompatActivity() {

    lateinit var viewModel: VM
    lateinit var binding: BINDING
    lateinit var loadingDialog: ProgressDialog;

    private var mLastClickTime: Long = 0

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!))
        fixTimeOut()
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding = DataBindingUtil.setContentView(this, getContentLayout())
        setContentView(binding.root)
        loadingDialog = ProgressDialog.newInstance()
        initViewModel()
        observerDefaultLiveData()
        initView()
        initListener()
        observerLiveData()
    }

    abstract fun getContentLayout(): Int

    abstract fun initViewModel()

    abstract fun initView()

    abstract fun initListener()

    abstract fun observerLiveData()

    open fun isDoubleClick(DOUBLE_PRESS_INTERVAL: Long): Boolean {
        if (SystemClock.elapsedRealtime() - mLastClickTime < DOUBLE_PRESS_INTERVAL) {
            return true
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        return false
    }

    open fun fixTimeOut() {
        try {
            val clazz = Class.forName("java.lang.Daemons\$FinalizerWatchdogDaemon")
            @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS") val method: Method =
                clazz.superclass.getDeclaredMethod("stop")
            method.isAccessible = true
            val field: Field = clazz.getDeclaredField("INSTANCE")
            field.isAccessible = true
            method.invoke(field.get(null))
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun observerDefaultLiveData() {
        viewModel.apply {
            isLoading.observe(this@BaseActivity, {
                if (it) {
                    showLoading()
                } else {
                    hideLoading()
                }
            })
            errorMessage.observe(this@BaseActivity, {
                if (it != null) {
                    showError(it.toInt())
                }
            })
            responseMessage.observe(this@BaseActivity, {
                showError(it.toString())
            })
        }

    }

    private fun showLoading() {
        if (!loadingDialog.isAdded)
            loadingDialog.show(supportFragmentManager, null)
    }

    private fun hideLoading() {
        if (loadingDialog.isVisible) {
            loadingDialog.dismiss()
        }
    }

    private fun showError(errorMessage: String) {
        val errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackbar.setAction("", null)
        errorSnackbar.show()
    }

    private fun showError(@StringRes id: Int) {
        val errorSnackbar = Snackbar.make(binding.root, id, Snackbar.LENGTH_LONG)
        errorSnackbar.setAction("", null)
        errorSnackbar.show()
    }
}
